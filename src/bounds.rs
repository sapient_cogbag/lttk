//! Traits representing how default bounds on input ranges are specified.
use crate::numprops;

/// Default boundaries for input ranges when not specified >.<
pub trait DefaultInputBounds<T: PartialEq + PartialOrd> {
    const DEFAULT_LOWER_BOUND: T;
    const DEFAULT_UPPER_BOUND: T;
    /// Whether or not a `[Self::DEFAULT_LOWER_BOUND] <= value` calculation must be done in the case
    /// that the default lower bound is the lower bound in a range.
    ///
    /// Essentially says if not ∀valid value∈\[T\],[Self::DEFAULT_LOWER_BOUND] <= value
    const NEEDS_LOWER_BOUND_CHECK: bool = true;
    /// Whether or not a `value <= [Self::DEFAULT_UPPER_BOUND]` calculation must be done in the case
    /// that the default upper bound is the upper bound
    ///
    /// Essentially says if not ∀valid value∈\[T\],value <= [Self::DEFAULT_UPPER_BOUND]
    const NEEDS_UPPER_BOUND_CHECK: bool = false;
}

/// Tag type implementing [DefaultInputBounds] based on [numprops::MinMaxNil],
/// in which the default lower and upper bounds are [numprops::MinMaxNil::MIN] and
/// [numprops::MinMaxNil::MAX] respectively.
pub struct MinMaxDefaultInputBounds;

impl<T: numprops::MinMaxNil + PartialEq + PartialOrd> DefaultInputBounds<T>
    for MinMaxDefaultInputBounds
{
    const DEFAULT_LOWER_BOUND: T = T::MIN;
    const DEFAULT_UPPER_BOUND: T = T::MAX;

    const NEEDS_LOWER_BOUND_CHECK: bool = false;
    const NEEDS_UPPER_BOUND_CHECK: bool = false;
}

/// Tag type implementing [DefaultInputBounds] based on [numprops::MinMaxNil],
/// in which the default lower and upper bounds are [numprops::MinMaxNil::ZERO] and
/// [numprops::MinMaxNil::MAX]
pub struct ZeroMaxDefaultInputBounds;

impl<T: numprops::MinMaxNil + PartialEq + PartialOrd> DefaultInputBounds<T>
    for ZeroMaxDefaultInputBounds
{
    const DEFAULT_LOWER_BOUND: T = T::ZERO;
    const DEFAULT_UPPER_BOUND: T = T::MAX;

    // For unsigned values, we should not need zero checking nya
    // However, we can't check using == or using the [Unsigned] trait at compile time :/
    const NEEDS_UPPER_BOUND_CHECK: bool = false;
}

/// Tag type implementing [DefaultInputBounds] based on [numprops::MinMaxNil],
/// in which the default lower and upper bounds are [numprops::MinMaxNil::MIN] and
/// [numprops::MinMaxNil::ZERO]
///
/// Note that for unsigned types this will likely have odd effects.
pub struct MinZeroDefaultInputBounds;

impl<T: numprops::MinMaxNil + PartialEq + PartialOrd> DefaultInputBounds<T>
    for MinZeroDefaultInputBounds
{
    const DEFAULT_LOWER_BOUND: T = T::MIN;
    const DEFAULT_UPPER_BOUND: T = T::ZERO;

    const NEEDS_LOWER_BOUND_CHECK: bool = false;
}

/// Tag type implementing [DefaultInputBounds] for floating point values using their INFINITY
/// and NEG_INFINITY constants.
///
/// Note that this should only be used if you care about infinity values being included in
/// ranges.
pub struct FPNegPosInfinityDefaultInputBounds;

macro_rules! fp_default_impls {
    ($fpty:ty) => {
        impl DefaultInputBounds<$fpty> for FPNegPosInfinityDefaultInputBounds {
            const DEFAULT_LOWER_BOUND: $fpty = <$fpty>::NEG_INFINITY;
            const DEFAULT_UPPER_BOUND: $fpty = <$fpty>::INFINITY;
        }
    };
}

fp_default_impls! {f32}
fp_default_impls! {f64}

//  Copyright © 2022 <sapient_cogbag at protonmail dot com>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy of this software
//  and associated documentation files (the “Software”), to deal in the Software without
//  restriction, including without limitation the rights to use, copy, modify, merge, publish,
//  distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
//  Software is furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all copies or
//  substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
//  BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.}
