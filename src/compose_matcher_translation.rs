//! Traits that enable the construction of a translation function with minimal runtime
//! cost. This module is oriented toward pairing together an individual matcher and translator.
use core::marker::PhantomData;

use crate::{
    bounds::{DefaultInputBounds, MinMaxDefaultInputBounds},
    delta::{DeltaApplicationOp, HasPositiveDelta, PlainAddSubOp},
    i_bounds::InputBounds,
    o_ranges::OutputRange,
    preconv::{AsOpConversion, PreConversion},
    ranges::{DefaultOutputRange, MinMaxDefaultOutputRange},
    rhs_tag,
};

/// Trait for despecificising types for single matches :3.
pub trait SingleMatchTranslate {
    /// Source type
    type Source;

    /// Target type
    type Target;

    /// Check if a value matches this match-translate.
    fn matches(&self, src_value: &Self::Source) -> bool;

    /// Convert a value that matched [Self::matches] into [Self::Target].
    ///
    /// # SAFETY REQUIREMENTS:
    ///  * Parameter *must* have returned true from [Self::matches] ^.^
    unsafe fn translate(&self, src_value: Self::Source) -> Self::Target;
}

/// Empty struct for all the phantomdatas
struct SingleMatchAndTranslateInnerPD<
    Source: HasPositiveDelta + PartialEq + PartialOrd,
    TargetDelta,
    Target,
    Bounds: InputBounds<Source>,
    Range: OutputRange<Target>,
    const IS_REVERSED: bool = false,
    // DeltaDirection: rhs_tag::TranslationTag = rhs_tag::Forward,
    DefaultBounds: DefaultInputBounds<Source> = MinMaxDefaultInputBounds,
    DefaultRange: DefaultOutputRange<Target> = MinMaxDefaultOutputRange,
    DeltaConversion: PreConversion<Source, TargetDelta> = AsOpConversion,
    Operation: DeltaApplicationOp<Target, Delta = TargetDelta> = PlainAddSubOp<TargetDelta>,
> {
    _ph_default_bounds: PhantomData<DefaultBounds>,
    _ph_default_range: PhantomData<DefaultRange>,
    _ph_matching: PhantomData<fn(&Source, DefaultBounds, &Bounds) -> bool>,
    _ph_transform: PhantomData<fn(Source) -> TargetDelta>,
    _ph_translate:
        PhantomData<fn(TargetDelta, Target, /*DeltaDirection,*/ DefaultRange, &Range) -> Target>,
    _ph_fn_delta_conversion: PhantomData<DeltaConversion>,
    _ph_operation: PhantomData<Operation>,
}

impl<
        Source: HasPositiveDelta + PartialEq + PartialOrd,
        TargetDelta,
        Target,
        Bounds: InputBounds<Source>,
        Range: OutputRange<Target>,
        /*DeltaDirection: rhs_tag::TranslationTag,*/
        const IS_REVERSED: bool,
        DefaultBounds: DefaultInputBounds<Source>,
        DefaultRange: DefaultOutputRange<Target>,
        DeltaConversion: PreConversion<Source, TargetDelta>,
        Operation: DeltaApplicationOp<Target, Delta = TargetDelta>,
    > Default
    for SingleMatchAndTranslateInnerPD<
        Source,
        TargetDelta,
        Target,
        Bounds,
        Range,
        /*DeltaDirection,*/
        IS_REVERSED,
        DefaultBounds,
        DefaultRange,
        DeltaConversion,
        Operation,
    >
{
    fn default() -> Self {
        Self {
            _ph_default_bounds: Default::default(),
            _ph_default_range: Default::default(),
            _ph_matching: Default::default(),
            _ph_transform: Default::default(),
            _ph_translate: Default::default(),
            _ph_fn_delta_conversion: Default::default(),
            _ph_operation: Default::default(),
        }
    }
}

/// Represents a single full set of `<Source, TargetDelta, Target, ..various type
/// parameters for specifying operations>` with a `.matches()` and `.translate()` function.
///
/// # GENERIC PARAMETERS:
/// * `Source`: Type to be conditionally translated
/// * `TargetDelta`: Intermediary type that differences in the source type are converted to before
///   being applied to one of the target output range bounds via a [DeltaApplicationOp]
/// * `Target`: Final output type (and type of the values in output range).
/// * `Bounds`: Input bound specifier type (defaults are defined for [core::ops::Range] and friends).
///   Implements [InputBounds]
/// * `Range`: Output range specifier type (defaults are defined for [core::ops::Range] and
///   non-upper-boundary-inclusive friends). Implements [OutputRange]
/// * `IS_REVERSED`: bool const generic. If false, the translation occurs from the output range start
///   forward towards the output range endpoint. If true, the translation occurs from the output
///   range end backward towards the output range startpoint. Default: false.
/// * `DefaultBounds`: [DefaultInputBounds<Source>] indicating what input bounds should be used by default when
///   otherwise unspecified ^.^ - Default: [MinMaxDefaultInputBounds], which uses the minimum and
///   maximum of (via `T::MIN` and `T::MAX`) of a given type as the lower and upper bounds,
///   respectively. For types that do not have these, define a custom tagtype and implement
///   [DefaultInputBounds<Source>] on it.
/// * `DefaultRange`: [DefaultOutputRange<Target>] indicating what output range endpoints should be
///   used by default. Default value is [MinMaxDefaultOutputRange], which uses `T::MIN` and `T::MAX`
///   as the start and endpoints when unspecified ^.^. However, it is important to remember that
///   output ranges and the output end of translation in general do not require any notion of
///   order, so if you supply a custom one of these you could use it for things like vector
///   transformations (with an appropriate [DeltaApplicationOp], anyhow).
/// * `DeltaConversion`: A [PreConversion<Source, Target>] specifying how changes in the source
///   should be converted to changes in the target. By default, it uses [AsOpConversion], which
///   just converts using the `as` builtin operator (for integers and floating point values). More
///   complex cases can use [crate::preconv::IntoOpConversion] or write their own.
/// * `Operation`: This type parameter indicates the operation by which a change in the target is
///   applied *to* a target ^.^, implementing [DeltaApplicationOp<Target>]. By default, it uses the
///   plain addition `+` and subtraction `-` operators as defined by [PlainAddSubOp], however many
///   other options exist by default (see: [crate::delta::WrappingAddSubOp] and
///   [crate::delta::SaturatingAddSubOp]). You can also define your own.
///
/// TODO: When rust lets you use constants from type parameters as other generic parameters (issue
///  switch back to [crate::rhs_tag::TranslationTag] from the const generic IS_REVERSED
pub struct SingleMatchAndTranslate<
    Source: HasPositiveDelta + PartialEq + PartialOrd,
    TargetDelta,
    Target,
    Bounds: InputBounds<Source>,
    Range: OutputRange<Target>,
    const IS_REVERSED: bool = false,
    // DeltaDirection: rhs_tag::TranslationTag = rhs_tag::Forward,
    DefaultBounds: DefaultInputBounds<Source> = MinMaxDefaultInputBounds,
    DefaultRange: DefaultOutputRange<Target> = MinMaxDefaultOutputRange,
    DeltaConversion: PreConversion<Source, TargetDelta> = AsOpConversion,
    Operation: DeltaApplicationOp<Target, Delta = TargetDelta> = PlainAddSubOp<TargetDelta>,
> {
    _ph: SingleMatchAndTranslateInnerPD<
        Source,
        TargetDelta,
        Target,
        Bounds,
        Range,
        /*DeltaDirection,*/
        IS_REVERSED,
        DefaultBounds,
        DefaultRange,
        DeltaConversion,
        Operation,
    >,
    /// The bounds we check against when matching
    match_check_bounds: Bounds,
    /// The output range.
    output_range: Range,
    /// The final operation to apply a calculated `TargetDelta` to `Target`
    final_application_operation: Operation,
}

impl<
        Source: HasPositiveDelta + PartialEq + PartialOrd,
        TargetDelta,
        Target,
        Bounds: InputBounds<Source>,
        Range: OutputRange<Target>,
        /*DeltaDirection: rhs_tag::TranslationTag,*/
        const IS_REVERSED: bool,
        DefaultBounds: DefaultInputBounds<Source>,
        DefaultRange: DefaultOutputRange<Target>,
        DeltaConversion: PreConversion<Source, TargetDelta>,
        Operation: DeltaApplicationOp<Target, Delta = TargetDelta>,
    > SingleMatchTranslate
    for SingleMatchAndTranslate<
        Source,
        TargetDelta,
        Target,
        Bounds,
        Range,
        /*DeltaDirection,*/
        IS_REVERSED,
        DefaultBounds,
        DefaultRange,
        DeltaConversion,
        Operation,
    >
{
    type Source = Source;
    type Target = Target;

    /// Check whether or not the bounds contain the given value, including with defaults and
    /// stuff.
    #[inline]
    fn matches(&self, value: &Source) -> bool {
        self.match_check_bounds
            .lower_bound_compare::<DefaultBounds>(value)
            && self
                .match_check_bounds
                .upper_bound_compare::<DefaultBounds>(value)
    }

    /// Perform a final translation - note that this requires that the argument value made
    /// `true` be returned from [Self::matches].
    ///
    /// # SAFETY REQUIREMENTS:
    /// * Passed value returned `true` when passed to [Self::matches]
    #[inline]
    unsafe fn translate(&self, value: Source) -> Target {
        // SAFETY: Matching ensures value > the match lower bound ^.^ nya
        let src_delta = unsafe {
            self.match_check_bounds
                .positive_sub_from::<DefaultBounds>(value)
        };
        let target_delta = DeltaConversion::convert(src_delta);
        // SAFETY: Use same IS_REVERSED parameters.
        unsafe {
            let apply_op_in_direction =
                rhs_tag::translate_delta_partial::<IS_REVERSED, Target, Operation>(
                    &self.final_application_operation,
                    target_delta,
                );
            self.output_range
                .apply_operation_in_direction::<DefaultRange, IS_REVERSED>(apply_op_in_direction)
        }
    }
}

impl<
        Source: HasPositiveDelta + PartialEq + PartialOrd,
        TargetDelta,
        Target,
        Bounds: InputBounds<Source>,
        Range: OutputRange<Target>,
        /*DeltaDirection: rhs_tag::TranslationTag,*/
        const IS_REVERSED: bool,
        DefaultBounds: DefaultInputBounds<Source>,
        DefaultRange: DefaultOutputRange<Target>,
        DeltaConversion: PreConversion<Source, TargetDelta>,
        Operation: DeltaApplicationOp<Target, Delta = TargetDelta>,
    >
    SingleMatchAndTranslate<
        Source,
        TargetDelta,
        Target,
        Bounds,
        Range,
        /*DeltaDirection,*/
        IS_REVERSED,
        DefaultBounds,
        DefaultRange,
        DeltaConversion,
        Operation,
    >
{
    /// New function where you specify params! ^.^
    /// * `check_bounds`: Input bounds
    /// * `out_range`: Output range specifier
    /// * `delta_application_op`: operation to apply a change to the target.
    ///
    /// See [build] for more user-friendly construction functions (this one is really janky to use
    /// due to all the types you are going to want to infer automatically as defaults in most
    /// cases). It also has a useful alternative in [Self::new_default_op] which will automatically
    /// fill in operations that implement [Default::default], which is most of them since they tend
    /// to be tag types.
    ///
    /// Note - this type has a LOT of type parameters ^.^, many of which are used to define
    /// behaviour (luckily most have sensible defaults). Setting them individually is doable,
    /// though it's recommended to let rust deduce the first few from the parameters passed here.
    /// See [Self] for the documentation of the type parameters.
    ///
    /// To make type parameters easier to handle, we have individual methods for them so you can do
    /// a simple ```let translation = SingleMatchAndTranslate::<Source, TargetDelta, Target, _,
    /// _>::new(...)``` and then use the `.with()` transformation methods.
    #[inline]
    pub fn new(check_bounds: Bounds, out_range: Range, delta_application_op: Operation) -> Self {
        Self {
            _ph: Default::default(),
            match_check_bounds: check_bounds,
            output_range: out_range,
            final_application_operation: delta_application_op,
        }
    }

    /// Like [Self::new] but fills in operations implementing [Default::default] that are tag types
    /// (most of them ^.^)
    #[inline]
    pub fn new_default_op(check_bounds: Bounds, out_range: Range) -> Self
    where
        Operation: Default,
    {
        Self::new(check_bounds, out_range, Operation::default())
    }

    /// Change the operation used in the translation, as well as (as a consequence) the conversion
    /// operator to produce input for the final op as well as the explicit new `TargetDelta`
    #[inline]
    pub fn with_preconv_and_operation<
        NewConversion: PreConversion<Source, NewOperation::Delta>,
        NewTargetDelta,
        NewOperation: DeltaApplicationOp<Target, Delta = TargetDelta>,
    >(
        self,
        operation: NewOperation,
    ) -> SingleMatchAndTranslate<
        Source,
        NewOperation::Delta,
        Target,
        Bounds,
        Range,
        IS_REVERSED,
        DefaultBounds,
        DefaultRange,
        NewConversion,
        NewOperation,
    > {
        SingleMatchAndTranslate {
            _ph: Default::default(),
            match_check_bounds: self.match_check_bounds,
            output_range: self.output_range,
            final_application_operation: operation,
        }
    }

    /// Same as [Self::with_preconv_and_operation], but lets you provide the operation purely in
    /// the type signature when it implements [Default], since most [DeltaApplicationOp] are simple
    /// tag types.
    #[inline]
    pub fn with_preconv_and_default_op<
        NewConversion: PreConversion<Source, NewOperation::Delta>,
        NewTargetDelta,
        NewOperation: DeltaApplicationOp<Target, Delta = TargetDelta> + Default,
    >(
        self,
    ) -> SingleMatchAndTranslate<
        Source,
        NewOperation::Delta,
        Target,
        Bounds,
        Range,
        IS_REVERSED,
        DefaultBounds,
        DefaultRange,
        NewConversion,
        NewOperation,
    > {
        self.with_preconv_and_operation::<NewConversion, TargetDelta, NewOperation>(
            NewOperation::default(),
        )
    }

    /// Change just the operation used to translate target endpoints. Note - unlike
    /// [Self::with_preconv_and_operation], which can change the intermediary `TargetDelta` type to
    /// accommodate a new operation, this function is restricted to operations consuming the same
    /// `TargetDelta` as the current operation.
    #[inline]
    pub fn with_operation<NewOperation: DeltaApplicationOp<Target, Delta = TargetDelta>>(
        self,
        op: NewOperation,
    ) -> SingleMatchAndTranslate<
        Source,
        TargetDelta,
        Target,
        Bounds,
        Range,
        IS_REVERSED,
        DefaultBounds,
        DefaultRange,
        DeltaConversion,
        NewOperation,
    > {
        SingleMatchAndTranslate {
            _ph: Default::default(),
            match_check_bounds: self.match_check_bounds,
            output_range: self.output_range,
            final_application_operation: op,
        }
    }

    /// Like [Self::with_operation] but allows for specifying the operation in the type parameter
    /// only if it implements [Default]. Useful since most [DeltaApplicationOp]s are just plain tag
    /// types that do implement [Default].
    #[inline]
    pub fn with_default_op<
        NewOperation: DeltaApplicationOp<Target, Delta = TargetDelta> + Default,
    >(
        self,
    ) -> SingleMatchAndTranslate<
        Source,
        TargetDelta,
        Target,
        Bounds,
        Range,
        IS_REVERSED,
        DefaultBounds,
        DefaultRange,
        DeltaConversion,
        NewOperation,
    > {
        self.with_operation(NewOperation::default())
    }

    #[inline]
    /// Change the [DefaultInputBounds] this uses. See [Self] for what that is.
    pub fn with_default_bounds<NewDefaultBounds: DefaultInputBounds<Source>>(
        self,
    ) -> SingleMatchAndTranslate<
        Source,
        TargetDelta,
        Target,
        Bounds,
        Range,
        /*DeltaDirection,*/
        IS_REVERSED,
        NewDefaultBounds,
        DefaultRange,
        DeltaConversion,
        Operation,
    > {
        SingleMatchAndTranslate {
            _ph: Default::default(),
            match_check_bounds: self.match_check_bounds,
            output_range: self.output_range,
            final_application_operation: self.final_application_operation,
        }
    }

    #[inline]
    /// Change the [DefaultOutputRange] this uses. See [Self] for what that is.
    pub fn with_default_range<NewDefaultRange: DefaultOutputRange<Target>>(
        self,
    ) -> SingleMatchAndTranslate<
        Source,
        TargetDelta,
        Target,
        Bounds,
        Range,
        /*DeltaDirection,*/
        IS_REVERSED,
        DefaultBounds,
        NewDefaultRange,
        DeltaConversion,
        Operation,
    > {
        SingleMatchAndTranslate {
            _ph: Default::default(),
            match_check_bounds: self.match_check_bounds,
            output_range: self.output_range,
            final_application_operation: self.final_application_operation,
        }
    }

    #[inline]
    /// This, but as a reverse-translation. See [above documentation][Self]'s information on
    /// `IS_REVERSED` for information.
    pub fn as_reversed_translation(
        self,
    ) -> SingleMatchAndTranslate<
        Source,
        TargetDelta,
        Target,
        Bounds,
        Range,
        /*DeltaDirection,*/
        true,
        DefaultBounds,
        DefaultRange,
        DeltaConversion,
        Operation,
    > {
        SingleMatchAndTranslate {
            _ph: Default::default(),
            match_check_bounds: self.match_check_bounds,
            output_range: self.output_range,
            final_application_operation: self.final_application_operation,
        }
    }

    #[inline]
    /// This, but as a forward-translation. See [above documentation][Self]'s information on
    /// `IS_REVERSED` for information ^.^
    pub fn as_forward_translation(
        self,
    ) -> SingleMatchAndTranslate<
        Source,
        TargetDelta,
        Target,
        Bounds,
        Range,
        /*DeltaDirection,*/
        false,
        DefaultBounds,
        DefaultRange,
        DeltaConversion,
        Operation,
    > {
        SingleMatchAndTranslate {
            _ph: Default::default(),
            match_check_bounds: self.match_check_bounds,
            output_range: self.output_range,
            final_application_operation: self.final_application_operation,
        }
    }
}

/// Module to ease the construction of [SingleMatchAndTranslate] structures (they have a LOT of
/// type parameters and the lack of being able to do `Name = Type` for defaults makes them quite
/// hard to construct non-painfully).
///
/// For simple cases where you mostly want to keep the original operations and config, it's worth looking at
/// the following methods - they're also useful if you for some reason find a gap in the [build]
/// API:
///  * [`SingleMatchAndTranslate::with_preconv_and_operation`]
///  * [`SingleMatchAndTranslate::with_preconv_and_default_op`]
///  * [`SingleMatchAndTranslate::with_operation`]
///  * [`SingleMatchAndTranslate::with_default_op`]
///  * [`SingleMatchAndTranslate::with_default_bounds`]
///  * [`SingleMatchAndTranslate::with_default_range`]
///  * [`SingleMatchAndTranslate::as_reversed_translation`]
///  * [`SingleMatchAndTranslate::as_forward_translation`]
pub mod build {}

#[cfg(test)]
mod test {
    use crate::delta::WrappingAddSubOp;

    use super::*;
    #[test]
    fn basic_match_translation() {
        let translation =
            SingleMatchAndTranslate::<u8, u8, u8, _, _>::new_default_op(b'0'..=b'9', 0..9);
        for idx in 0x0u8..=0xFF {
            if b'0' <= idx && idx <= b'9' {
                let does_match = translation.matches(&idx);
                assert!(does_match, "idx {} did not match!", idx);
                unsafe {
                    assert_eq!(idx - b'0', translation.translate(idx));
                } // We just assert!ed
            } else {
                // No matches here!
                assert!(!translation.matches(&idx));
            }
        }
    }

    #[test]
    fn basic_reverse_translation() {
        let rev_translation =
            SingleMatchAndTranslate::<u8, u8, u8, _, _, true>::new_default_op(b'0'..=b'9', 0..9);
        for idx in 0x0u8..=0xFF {
            if b'0' <= idx && idx <= b'9' {
                let does_match = rev_translation.matches(&idx);
                assert!(does_match, "idx {} did not match!", idx);
                unsafe {
                    assert_eq!(b'9' - idx, rev_translation.translate(idx));
                } // We just assert!ed
            } else {
                // No matches here!
                assert!(!rev_translation.matches(&idx));
            }
        }
    }

    #[test]
    fn basic_with_wrapping() {
        let translation =
            SingleMatchAndTranslate::<u8, u8, u8, _, _>::new_default_op(0x00..=0x7F, 0xFF..)
                .with_default_op::<WrappingAddSubOp<_>>();
        unsafe {
            // range is 0-127 inclusive.
            assert_eq!(translation.translate(0), 0xFF);
            assert_eq!(translation.translate(1), 0x00);
            assert_eq!(translation.translate(5), 0x04);
            assert_eq!(translation.translate(0x7F), 0x7E);
        }
    }
}

//  Copyright © 2022 <sapient_cogbag at protonmail dot com>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy of this software
//  and associated documentation files (the “Software”), to deal in the Software without
//  restriction, including without limitation the rights to use, copy, modify, merge, publish,
//  distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
//  Software is furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all copies or
//  substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
//  BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.}
