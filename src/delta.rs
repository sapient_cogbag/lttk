//! When performing translations, a difference is calculated in the source type, as well as
//! the target type (that one is more dangerous).
//!
//! This dictates the method to do so in the source type to avoid losing information due to type bounds etc.

use core::marker::PhantomData;
use core::ops::Add;
use core::ops::Sub;

use crate::numprops::SaturatingAdd;
use crate::numprops::SaturatingSub;
use crate::numprops::WrappingAdd;
use crate::numprops::WrappingSub;

/// Indicates the type to use when subtracting two values of the implementing type for which
/// A - B has B <= A - i.e such that the result is always positive - as well as a method to do
/// so without losing information.
pub trait HasPositiveDelta {
    type PositiveDelta;

    /// Method of subtracting that preferrably does not lose information if the result is too big for the
    /// originating type, when the result is guarunteed to be positive (or rather, that the
    /// subtracted value is always <= to the original value)
    ///
    /// For floating point values, this may not be so possible to avoid losing information, and
    /// it is considered acceptable to do so because a float overflow is extremely large and
    /// floating point operations tend to lose information *anyway*.
    ///
    /// Recommended in most cases - #\[inline\]
    ///
    /// SAFETY CONTRACT - other must be <= self.
    unsafe fn positive_subtract(self, other: Self) -> Self::PositiveDelta;
}

/// Implementation for copy types and references that allows for positive subtraction stuff.
impl<T: Copy + HasPositiveDelta> HasPositiveDelta for &'_ T {
    type PositiveDelta = T::PositiveDelta;

    #[inline]
    unsafe fn positive_subtract(self, other: Self) -> Self::PositiveDelta {
        // SAFETY: Same trait, same safety requirements on outer function nya ^.^
        unsafe { T::positive_subtract(*self, *other) }
    }
}

// Implementations for signed and unsigned nya
macro_rules! posdelta_unsigned_impl {
    ($($unsigned_ty:ty)*) => {$(
        impl HasPositiveDelta for $unsigned_ty {
            type PositiveDelta = $unsigned_ty;

            #[inline]
            unsafe fn positive_subtract(self, other: Self) -> Self::PositiveDelta {
                self - other
            }
        }
    )*}
}

posdelta_unsigned_impl! {u8 u16 u32 u64 u128 usize}

// Signed implementation of safe-positive-subtract is a little harder
//
// Signed numbers in rust are guarunteed to be represented in two's complement when transformed
// via `as` into their unsigned counterparts nya. As such, we need to work with two's
// complement to make this function.
//
// First important part is to use .wrapping_sub so that desired wrapping behaviour happens.
// When the difference between two signed integer values is less than the size of the type,
// things are simple - the bit level representation is the same as for the unsigned variation
// of the type.
//
// For values >= type size, however, things are a little more complex. In particular, wrapping
// subtraction will result in it coming right around to the *most negative* value, which is the
// smallest value for the signed type with a most-significant bit that is set. Further
// increases in difference also increase the value of the corresponding unsigned type nya.
// Thus, wrapping_sub + an `as <unsized>` works well.
macro_rules! posdelta_signed_impl {
    ($($signed_ty:ty => $unsigned_ty:ty),*) => {$(
        impl HasPositiveDelta for $signed_ty {
            type PositiveDelta = $unsigned_ty;

            #[inline]
            unsafe fn positive_subtract(self, other: Self) -> Self::PositiveDelta {
                self.wrapping_sub(other) as $unsigned_ty
            }
        }
    )*}
}

posdelta_signed_impl! {i8=>u8, i16=>u16, i32=>u32, i64=>u64, i128=>u128, isize=>usize}

// Floating-point implementations
macro_rules! posdelta_fp_impl {
    ($($fpty:ty)*) => {$(
        /// Warning - This is not perfectly information-conserving, but floating point
        /// operations generally are not information-conserving anyway.
        impl HasPositiveDelta for $fpty {
            type PositiveDelta = $fpty;

            #[inline]
            unsafe fn positive_subtract(self, other: Self) -> Self::PositiveDelta {
                self - other
            }
        }
    )*}
}

posdelta_fp_impl! {f32}
posdelta_fp_impl! {f64}

#[cfg(test)]
mod test {
    use super::HasPositiveDelta;
    macro_rules! safesub_rules{
        ($($src_ty:ty => $positive_delta_ty:ty),*) => {$(
            //println!(concat!("Checking HasPositiveDelta subtraction for", $src_ty, " (PositiveDelta: ", $positive_delta_ty, ")"));

            // Max-Min
            assert_eq!(<$src_ty as HasPositiveDelta>::positive_subtract(<$src_ty>::MAX, <$src_ty>::MIN), <$positive_delta_ty>::MAX);

            // -5 and zero
            assert_eq!(<$src_ty as HasPositiveDelta>::positive_subtract(0, -5), 5);
        )*}
    }

    #[test]
    fn integer_safe_subtract() {
        unsafe {
            safesub_rules! {i8=>u8, i16=>u16, i32=>u32, i64=>u64, i128=>u128, isize=>usize}
        }
    }
}

/// Operation for applying a change to a target. May be adding/subtracting,
/// wrapping_add/wrapping_sub, etc.
pub trait DeltaApplicationOp<Target> {
    /// The type that should be accepted as a Delta for the target
    type Delta;

    /// Apply the delta to the target, treating the delta as an "adding" change
    fn add_delta(&self, initial_value: Target, delta: Self::Delta) -> Target;

    /// Apply the delta to the target, treating the delta as a change to be "subtracted"
    fn sub_delta(&self, initial_value: Target, delta: Self::Delta) -> Target;
}

/// Tag type indicating that the delta application operation should be a plain + or -
/// operation with a delta of the given parameter.
#[derive(Default, Clone, Copy)]
pub struct PlainAddSubOp<DeltaType>(PhantomData<DeltaType>);

impl<T, Q> DeltaApplicationOp<T> for PlainAddSubOp<Q>
where
    T: Add<Q, Output = T>,
    T: Sub<Q, Output = T>,
{
    type Delta = Q;

    #[inline]
    fn add_delta(&self, initial_value: T, delta: Self::Delta) -> T {
        initial_value + delta
    }

    #[inline]
    fn sub_delta(&self, initial_value: T, delta: Self::Delta) -> T {
        initial_value - delta
    }
}

/// Tag type indicating that the delta application operation should use wrapping arithmetic.
/// Any type implementing [crate::numprops::WrappingAdd] and [crate::numprops::WrappingSub] can be used here.
#[derive(Default, Clone, Copy)]
pub struct WrappingAddSubOp<DeltaType>(PhantomData<DeltaType>);

impl<T, Q> DeltaApplicationOp<T> for WrappingAddSubOp<Q>
where
    T: WrappingAdd<Q, Output = T>,
    T: WrappingSub<Q, Output = T>,
{
    type Delta = Q;

    #[inline]
    fn add_delta(&self, initial_value: T, delta: Self::Delta) -> T {
        initial_value.wrapping_add(delta)
    }

    #[inline]
    fn sub_delta(&self, initial_value: T, delta: Self::Delta) -> T {
        initial_value.wrapping_sub(delta)
    }
}

/// Tag type indicating that the delta application operation should use saturating arithmetic.
/// Any type implementing [crate::numprops::SaturatingAdd] and [crate::numprops::SaturatingSub] can be used here.
#[derive(Default, Clone, Copy)]
pub struct SaturatingAddSubOp<DeltaType>(PhantomData<DeltaType>);

impl<T, Q> DeltaApplicationOp<T> for SaturatingAddSubOp<Q>
where
    T: SaturatingAdd<Q, Output = T>,
    T: SaturatingSub<Q, Output = T>,
{
    type Delta = Q;

    #[inline]
    fn add_delta(&self, initial_value: T, delta: Self::Delta) -> T {
        initial_value.saturating_add(delta)
    }

    #[inline]
    fn sub_delta(&self, initial_value: T, delta: Self::Delta) -> T {
        initial_value.saturating_sub(delta)
    }
}

//  Copyright © 2022 <sapient_cogbag at protonmail dot com>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy of this software
//  and associated documentation files (the “Software”), to deal in the Software without
//  restriction, including without limitation the rights to use, copy, modify, merge, publish,
//  distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
//  Software is furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all copies or
//  substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
//  BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.}
