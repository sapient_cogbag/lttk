//! Input bounds specification
//!
//! This uses rust's default mechanism with [core::ops::Range] style stuff ^.^, but implementing
//! our own traits for avoiding comparisons at compile time reasons using TRAITS.
use crate::{bounds::DefaultInputBounds, delta::HasPositiveDelta};

/// General trait allowing generic, minimal-runtime selection of the method of applying comparisons
/// and performing the subtraction from the checked value without needing to do runtime checks if
/// something needs default replacement or not - rust already uses types to distinguish ranges,
pub trait InputBounds<SourceType: HasPositiveDelta + PartialEq + PartialOrd>
where
    for<'a> &'a SourceType: PartialEq + PartialOrd,
{
    /// Perform the appropriate comparison with the lower bound, returning true if the value is
    /// not excluded by the lower bound and false if it is excluded by the lower bound.
    ///
    /// Defaults are provided as a type parameter so that ranges unbounded on one end or the
    /// other can use them instead (as well as with optimisations based on the
    /// [DefaultInputBounds::NEEDS_LOWER_BOUND_CHECK] associated constant.
    fn lower_bound_compare<Defaults: DefaultInputBounds<SourceType>>(
        &self,
        value: &SourceType,
    ) -> bool;

    /// Perform the appropriate comparison with the upper bound, returning true if the value is
    /// not excluded by the upper bound and false if it is excluded by the upper bound.
    ///
    /// Defaults are provided as a type parameter so that ranges unbounded on one end or the
    /// other can use them instead (as well as with optimisations based on the
    /// [DefaultInputBounds::NEEDS_UPPER_BOUND_CHECK] associated constant.
    fn upper_bound_compare<Defaults: DefaultInputBounds<SourceType>>(
        &self,
        value: &SourceType,
    ) -> bool;

    /// Perform a positive subtraction of the lower bound on a value that has already been matched to within the
    /// bounds, with the subtracted value being the lower bound of this range ^.^
    ///
    /// # SAFETY REQUIREMENTS
    /// The passed value MUST have returned true from [Self::lower_bound_compare] and 
    /// [Self::upper_bound_compare] - i.e. it's within the bounds.
    unsafe fn positive_sub_from<Defaults: DefaultInputBounds<SourceType>>(
        &self,
        value: SourceType,
    ) -> SourceType::PositiveDelta;
}

// Implementations for core::ops::Range* types nya ^.^

/// Has both endpoints, max not included.
impl<T: HasPositiveDelta + PartialEq + PartialOrd + Copy> InputBounds<T> for core::ops::Range<T>
where
    for<'a> &'a T: PartialEq + PartialOrd,
{
    #[inline]
    fn lower_bound_compare<Defaults: DefaultInputBounds<T>>(&self, value: &T) -> bool {
        &self.start <= value
    }

    #[inline]
    fn upper_bound_compare<Defaults: DefaultInputBounds<T>>(&self, value: &T) -> bool {
        value < &self.end
    }

    #[inline]
    unsafe fn positive_sub_from<Defaults: DefaultInputBounds<T>>(
        &self,
        value: T,
    ) -> T::PositiveDelta {
        // SAFETY: have to return true from both other functions so value is definitely >=
        // self.start ;p
        unsafe {value.positive_subtract(self.start)}
    }
}

/// Has upper endpoint, not included.
impl<T: HasPositiveDelta + PartialEq + PartialOrd> InputBounds<T> for core::ops::RangeTo<T>
where
    for<'a> &'a T: PartialEq + PartialOrd,
{
    #[inline]
    fn lower_bound_compare<Defaults: DefaultInputBounds<T>>(&self, value: &T) -> bool {
        !Defaults::NEEDS_LOWER_BOUND_CHECK || &Defaults::DEFAULT_LOWER_BOUND <= value
    }

    #[inline]
    fn upper_bound_compare<Defaults: DefaultInputBounds<T>>(&self, value: &T) -> bool {
        value < &self.end
    }

    #[inline]
    unsafe fn positive_sub_from<Defaults: DefaultInputBounds<T>>(
        &self,
        value: T,
    ) -> T::PositiveDelta {
        // SAFETY: have to return true from both other functions so value is definitely >=
        // default lower bound - as long as NEEDS_LOWER_BOUND_CHECK is correct.
        unsafe { value.positive_subtract(Defaults::DEFAULT_LOWER_BOUND) }
    }
}

/// Has both endpoints, max included.
impl<T: HasPositiveDelta + PartialEq + PartialOrd + Copy> InputBounds<T>
    for core::ops::RangeInclusive<T>
where
    for<'a> &'a T: PartialEq + PartialOrd,
{
    #[inline]
    fn lower_bound_compare<Defaults: DefaultInputBounds<T>>(&self, value: &T) -> bool {
        self.start() <= value
    }

    #[inline]
    fn upper_bound_compare<Defaults: DefaultInputBounds<T>>(&self, value: &T) -> bool {
        value <= self.end()
    }

    #[inline]
    unsafe fn positive_sub_from<Defaults: DefaultInputBounds<T>>(
        &self,
        value: T,
    ) -> T::PositiveDelta {
        // SAFETY: Have to return true from above two fns, so value >= self.start()
        unsafe { value.positive_subtract(*self.start()) }
    }
}

/// Has upper endpoint, included.
impl<T: HasPositiveDelta + PartialEq + PartialOrd> InputBounds<T> for core::ops::RangeToInclusive<T>
where
    for<'a> &'a T: PartialEq + PartialOrd,
{
    #[inline]
    fn lower_bound_compare<Defaults: DefaultInputBounds<T>>(&self, value: &T) -> bool {
        !Defaults::NEEDS_LOWER_BOUND_CHECK || &Defaults::DEFAULT_LOWER_BOUND <= value
    }

    #[inline]
    fn upper_bound_compare<Defaults: DefaultInputBounds<T>>(&self, value: &T) -> bool {
        value <= &self.end
    }

    #[inline]
    unsafe fn positive_sub_from<Defaults: DefaultInputBounds<T>>(
        &self,
        value: T,
    ) -> T::PositiveDelta {
        // SAFETY: true from above two functions - As long as NEEDS_LOWER_BOUND_CHECK is valid,
        // this is valid ^.^ nya
        unsafe { value.positive_subtract(Defaults::DEFAULT_LOWER_BOUND) }
    }
}

/// Has lower endpoint, included. Has no upper so we use the default maximum inclusively
impl<T: HasPositiveDelta + PartialEq + PartialOrd + Copy> InputBounds<T> for core::ops::RangeFrom<T>
where
    for<'a> &'a T: PartialEq + PartialOrd,
{
    #[inline]
    fn lower_bound_compare<Defaults: DefaultInputBounds<T>>(&self, value: &T) -> bool {
        value <= &self.start
    }

    #[inline]
    fn upper_bound_compare<Defaults: DefaultInputBounds<T>>(&self, value: &T) -> bool {
        // No upper bound, use defaults to determine check necessity nya
        !Defaults::NEEDS_UPPER_BOUND_CHECK || value <= &Defaults::DEFAULT_UPPER_BOUND
    }

    #[inline]
    unsafe fn positive_sub_from<Defaults: DefaultInputBounds<T>>(
        &self,
        value: T,
    ) -> T::PositiveDelta {
        // SAFETY: both functions above must return true so val >= self.start ^.^ nya
        unsafe { value.positive_subtract(self.start) }
    }
}

/// Has neither endpoint, so we simply use the constants on Defaults all the time ^.^
impl<T: HasPositiveDelta + PartialEq + PartialOrd> InputBounds<T> for core::ops::RangeFull
where
    for<'a> &'a T: PartialEq + PartialOrd,
{
    #[inline]
    fn lower_bound_compare<Defaults: DefaultInputBounds<T>>(&self, value: &T) -> bool {
        !Defaults::NEEDS_LOWER_BOUND_CHECK || &Defaults::DEFAULT_LOWER_BOUND <= value
    }

    #[inline]
    fn upper_bound_compare<Defaults: DefaultInputBounds<T>>(&self, value: &T) -> bool {
        // No upper bound, use defaults to determine check necessity nya
        !Defaults::NEEDS_UPPER_BOUND_CHECK || value <= &Defaults::DEFAULT_UPPER_BOUND
    }

    #[inline]
    unsafe fn positive_sub_from<Defaults: DefaultInputBounds<T>>(
        &self,
        value: T,
    ) -> T::PositiveDelta {
        // SAFETY: both functions above must return true so as long as NEEDS_LOWER_BOUND_CHECK is
        // valid this meets requirements meow
        unsafe { value.positive_subtract(Defaults::DEFAULT_LOWER_BOUND) }
    }
}

//  Copyright © 2022 <sapient_cogbag at protonmail dot com>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy of this software
//  and associated documentation files (the “Software”), to deal in the Software without
//  restriction, including without limitation the rights to use, copy, modify, merge, publish,
//  distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
//  Software is furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all copies or
//  substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
//  BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.}
