#![no_std]
#![feature(doc_cfg)]
#![deny(unsafe_op_in_unsafe_fn)]
// Lets `impl Trait`s be used in functions with provided generic arguments (types or const nyaa ^.^)
#![feature(explicit_generic_args_with_impl_trait)]
//! This library is designed to check ranges and convert linear regions of values within and
//! between types - with a focus on discrete types like [u8] or [i16], but capability in
//! output types as well (for instance, with appropriate traits, you could take float inputs
//! and convert them to output lines in a space of vectors).
//!
//! It's most well-tested purpose is for converting character-bytes into numbers or
//! other characters, for instance mapping `b'0' to b'9'` => `0 to 9` and
//! `b'a' to b'f'` => `10 to 15` for a basic hexadecimal encoding, which I call
//! translation🏳‍⚧️/u

pub mod primitive_ranges;

// Core concepts - positive deltas, numerical properties, and type conversions
pub mod delta;
pub mod numprops;
pub mod preconv;

// Basic dictation of the direction of a translation ^.^
pub mod rhs_tag;

// Input bounds - defaults, and practical instantiation of them for ranges specifying input.
pub mod bounds;
pub mod i_bounds;

// Output bounds - defaults, and practical instantiation of them for ranges specifying output.
pub mod o_ranges;
pub mod ranges;

// Constructing individual match-translate as well as in-bulk
pub mod compose_matcher_translation;
pub mod trans;

/*
/// Macro for performing efficient character translation🏳‍⚧️//numerical translations using matches.
///
/// This macro is designed to be a very powerful method of translating regions of a type's valid
/// space (typically characters) into another region.
///
/// Input and output range equality is checked when both output upper and lower bounds are
/// provided. This check is done *at compile time*
///
/// ## SOURCE AND TARGET RANGES
/// When a list of source ranges is provided, an input value is checked for membership in each one
/// sequentially (with the first matching translation provided being used). All ranges include
/// their endpoints.
///
/// The output range is known as the target range, and it can either be in up mode (default)
/// or down mode (up and down modes can be set explicitly with 'up' and 'down' placed in front of
/// the target range).
///
/// The default (up mode) maps the first character/number in the source range to the
/// first character/number in target range, with each value beyond that in the source range
/// corresponding to the next value in the target range as well.
///
/// Down mode, however, maps the first character/number in the source range to the last element in
/// the target range, with each increase in the value matching the source range corresponding to a
/// decrease in value emitted from the target range.
///
/// In up mode, target ranges can be specified only with their lower bound (however, it is not
/// recommended as the upper bound allows extra checks). Not providing the *lower bound* will
/// result in it being set to 0.
///
/// In down mode, target ranges can be specified only with their upper bound (however, it is not
/// recommended as the lower bound allows extra checks). Not providing the upper bound will result
/// in it essentially being treated as value_type::MAX
///
///
///
///
/// ## Custom numeric types
/// If you have custom numeric types, you need to ensure they have a ::MAX and ::MIN type
/// constants like all of the standard rust types, as well as a + and - operator, >, >=, <, <=
/// comparators too.
///
///
#[macro_export]
macro_rules! translations {
    ($val:expr => <$sourcetype:ty => $targettype:ty> $(
        $($lowerbound:literal)? => $($upperbound:literal)?
        trans $($up_or_down:ident)? $($lowerout:literal)? => $($upperout:literal)?
        $(overunder $overunderflow:ident)?
    ),*
    $(failmode $failmode:ident)?
    ) => {{
        /* use ::core::{
            u8::MIN, u16::MIN, u32::MIN, u64::MIN, u128::MIN,
            i8::MIN, i16::MIN, i32::MIN, i64::MIN, i128::MIN
        };*/
        // Assertions of range equality in cases where we do so
        $(translations!(@debugrange <$sourcetype => $targettype>
            ($($lowerbound)? => $($upperbound)?) => $($lowerout)? ~ $($upperout)?
        );)*
        // Macro weirdness here because having a ? inside a * that is defined outside
        // the * does not work nya
        translations!(
            @failmode $($failmode)?
            @match $val {$(
                v if $crate::ranges!(v in <$sourcetype> $($lowerbound)? ~ $($upperbound)?)
                    => ::core::result::Result::Ok({
                    // Translation can go in one of two directions
                    // up or down
                    //
                    // Up translation means the source range increasing means the output is
                    // increasing - it is the default.
                    //
                    // Down translation means the source range increasing causes the output
                    // range to decrease
                    //
                    // Note that we use *wrapping_sub* and *wrapping_add* for calculations of the
                    // change to avoid serious annoyances - the final value is in fact
                    translations!(@calcfunction <$sourcetype => $targettype>
                        $($up_or_down)? @ $($overunderflow)? @
                        translations!(@outshift <$sourcetype> $($lowerbound)? => v $(ub $upperbound)?);
                        translations!(@targetexpression <$targettype> $($up_or_down)? @ $($lowerout)? => $($upperout)?)
                    )
                }),)*
                v @ _ => ::core::result::Result::Err(v)
            }
        )
    }};

    (@failmode option @ $val:expr) => {(
        $val.ok()
    )};

    (@failmode result @ $val:expr) => {(
        $val
    )};

    // Panic mode (default)
    (@failmode panic @ $val:expr) => {(
        $val.expect("Numerical value not in map")
    )};

    (@failmode @ $val:expr) => {( translations!(@failmode panic @ $val) )};

    // The amount to add to the lower output bound or subtract from the upper output
    // bound when in up or down mode, respectively nya. Because lower bounds are <= upper bounds,
    // and inputs are selected based on this, we know that the input is >= lower bound.
    //
    // This means we also don't need any fancy wrapping_add/wrapping_sub because we know it will
    // NEVER wrap
    (@outshift <$st:ty> $lowerbound:expr => $inval:ident $(ub $ub:literal)?) => {
        ($inval - $lowerbound)
    };
    // No lower bound provided, use sourcetype::min.
    (@outshift <$st:ty> => $inval:ident $(ub $ub:literal)?) => {
        translations!(@outshift <$st>::MIN => $inval $(ub $ub)?)
    };

    // Default when there's NOTHING AT ALL nyaaa
    //
    // Arguments to this submacro are the expression derived from source-type components
    // and expressions derived from the target type components.
    (@calcfunction <$st:ty => $tt:ty> @ @ $src:expr ; $target:expr) => {
          translations!(@calcfunction <$st => $tt> up @ plain @ $src ; $target)
    };
    // Default direction - up or down
    (@calcfunction <$st:ty => $tt:ty> @ $overflow_option:ident @ $src:expr ; $target:expr) => {
        translations!(@calcfunction <$st => $tt> up @ $overflow_option @ $src ; $target)
    };
    // Shift calculation functions - dependent on wrapping, saturating, or default.
    (@calcfunction <$st:ty => $tt:ty> $direction:ident @ @ $src:expr ; $target:expr) => {
        translations!(@calcfunction <$st => $tt> $direction @ plain @ $src ; $target)
    };

    // Plain/normal
    (@calcfunction <$st:ty => $tt:ty> up @ plain @ $src:expr ; $target:expr) => {
        ($target + $src)
    };
    (@calcfunction <$st:ty => $tt:ty> down @ plain @ $src:expr ; $target:expr) => {
        ($target - $src)
    };
    // Wrapping
    (@calcfunction <$st:ty => $tt:ty> up @ wrapping @ $src:expr ; $target:expr) => {
        ($tt::wrapping_add($target, ::core::convert::Into<$src) )
        (::core::convert::Into<<$target>>::into($src).wrapping_add($target))
    };
    (@calcfunction <$st:ty => $tt:ty> down @ wrapping @ $src:expr ; $target:expr) => {
        (::core::convert::Into<<$target>>::into($src).wrapping_sub($target))
    };
    // Saturating
    (@calcfunction <$st:ty => $tt:ty> up @ saturating @ $src:expr ; $target:expr) => {
        (::core::convert::Into<<$target>>::into($src).saturating_add($target))
    };
    (@calcfunction <$st:ty => $tt:ty> down @ saturating @ $src:expr ; $target:expr) => {
        (::core::convert::Into<<$target>>::into($src).saturating_sub($target))
    };

    // Obtains the expression for the target type from the upper and lower bounds, with
    // associated defaults
    //
    // First part indicates default direction
    (@targetexpression <$tt:ty> @ $($lo:expr)? => $($uo:expr)?) => {
        translations!(@targetexpression <$tt> up @ $($lo)? => $($uo)?)
    };
    // Next the defaults for upper-out-bounds (in the case of direction down) or
    // lower-out-bounds (in the case of direction up)
    (@targetexpression <$tt:ty> up @ => $($uo:expr)?) => {
        translations!(@targetexpression <$tt> up @ 0 => $($uo)?)
    };
    (@targetexpression <$tt:ty> down @ $($lo:expr)? => ) => {
        translations!(@targetexpression <$tt> down @ $($lo)?; <$tt>::MAX)
    };
    // Now for actual values nyaaaa
    // Note the expr here is because we might have obtained a ::MAX further up ;p
    //
    // This produces the expression from the target range that should be presented.
    // For UP mode, we want the lower output bound which is getting added on to nya
    // For DOWN mode, we want the upper output bound which is getting subtracted from.
    (@targetexpression <$tt:ty> up @ $lo:expr => $uo:expr) => { $lo };
    (@targetexpression <$tt:ty> down @ $lo:expr => $uo:expr) => { $uo };

    // Debug expressions time! nya
    //
    // Note that in the case of missing source range bounds, we still do checks by using the
    // defaults.
    //
    // In the case of the target range missing anything, do nothing.
    (@debugrange <$src:ty => $tgt:ty> (=> $($ub:expr)?) => $lo:literal ~ $uo:literal) => {
        translations!(@debugrange <$src => $tgt> (<$src>::MIN => $($ub)?) => $lo ~ $uo)
    };
    (@debugrange <$src:ty => $tgt:ty> ($lb:expr =>) => $lo:literal ~ $uo:literal) => {
        translations!(@debugrange <$src => $tgt> ($lb => <$src>::MAX) => $lo ~ $uo)
    };
    (@debugrange <$src:ty => $tgt:ty> ($lb:expr => $ub:expr) => $lo:literal ~ $uo:literal) => {
    #[allow(dead_code)]
    {/*
        // Allow dead code here since it's not actually dead.
        const INDELTA: $src = ($ub as $src).wrapping_add(1 as $src).wrapping_sub($lb);
        const OUTDELTA: $tgt = ($uo as $tgt).wrapping_add(1 as $tgt).wrapping_sub($lo);
        const IS_VALID: bool = (INDELTA as $tgt == OUTDELTA) && (INDELTA == OUTDELTA as $src);
        // Check the range differences are zero
        ::static_assertions::const_assert!(IS_VALID);
    */}};
    // Last case - no or only one provided endpoint literals, do not try to debug assert.
    (@debugrange <$src:ty => $tgt:ty> ($($lb:expr)? => $($ub:expr)?) =>$($lo:literal)? ~ $($uo:literal)?) => {};
}

#[cfg(test)]
mod translations_test {
    fn bin_chars(chr: u8) -> u8 {
        translations!(
            chr => <u8 => u8> b'0' => b'1' trans 0u8 => 1u8
        )
    }

    fn hex_chars_lower(chr: u8) -> u8 {
        translations!(
            chr => <u8 => u8>
                b'0' => b'9' trans 0 => 9,
                b'a' => b'f' trans 10 => 15
        )
    }

    fn hex_chars_mixed_case(chr: u8) -> u8 {
        translations!(
            chr => <u8 => u8>
                b'0' => b'9' trans 0 => 9,
                b'a' => b'f' trans 10 => 15,
                b'A' => b'F' trans 10 => 15
        )
    }

    #[test]
    #[should_panic]
    /// Test that the default error mode works.
    fn out_of_range_panic() {
        bin_chars(b'4');
    }

    #[test]
    /// Testing primitive functionality
    fn basic_single_functionality() {
        for b in 0..1 {
            assert_eq!(bin_chars(b'0' + b), b);
        }
    }

    #[test]
    /// Testing translations with multiple ranges available
    fn multi_ranges() {
        // First range nya (noninclusive
        for h in 0..10 {
            assert_eq!(hex_chars_lower(b'0' + h), h);
        }
        // Second range ^.^
        for h in 10..16 {
            assert_eq!(hex_chars_lower(b'a' + h - 10), h);
        }
    }

    #[test]
    /// Overlapping output ranges
    fn overlapping_out_ranges() {
        // First range nya (noninclusive
        for h in 0..10 {
            assert_eq!(hex_chars_mixed_case(b'0' + h), h);
        }
        // Second range ^.^
        for h in 10..16 {
            assert_eq!(hex_chars_mixed_case(b'a' + h - 10), h);
        }

        // Third range ^.^
        for h in 10..16 {
            assert_eq!(hex_chars_mixed_case(b'A' + h - 10), h);
        }
    }

    #[test]
    /// Overlapping input ranges - should have the first range listed be the first range used
    fn overlapping_input_ranges() {
        let trans = |v| {
            translations!(
                v => <u8 => u8>
                    0 => 4 trans 0 => 4,
                    3 => 8 trans 0 => 5
            )
        };

        for (src, tgt) in [(0, 0), (3, 3), (4, 4), (5, 2)] {
            assert_eq!(trans(src), tgt);
        }
    }

    /* Tests are compile time, so now it doesn't work
    mod range_length_checks {
        /// Testing asserts
        #[test]
        #[should_panic]
        fn invalid_long_ranges() {
            let trans = |v| translations!(
                v => <u8 => u8>
                    0 => 8 trans 10 => 19
                    failmode result
            );
            let _r = trans(230);
        }

        #[test]
        #[should_panic]
        fn invalid_long_ranges_arg_passes_through() {
            let trans = |v| translations!(
                v => <u8 => u8>
                    0 => 8 trans 10 => 19
                    failmode result
            );
            let _r = trans(2);
        }
    }
    */

    /// Testing default values for signed and unsigned
    mod defaults {
        #[test]
        fn unsigned_source_defaults() {
            let noupper = |v| {
                translations!(
                    v => <u8 => u8>
                    0x84 => trans 0 => 0x7B
                    failmode result
                )
            };

            assert_eq!(noupper(0x83), Err(0x83));
            assert_eq!(noupper(0x84), Ok(0));
            assert_eq!(noupper(0xFF), Ok(0xFF - 0x84));

            let nolower = |v| {
                translations!(
                    v => <u8 => u8>
                    => 0x6D trans 0x20 => 0x8D
                    failmode result
                )
            };

            assert_eq!(nolower(0x00), Ok(0x20));
            assert_eq!(nolower(0x6D), Ok(0x20 + 0x6D));
            assert_eq!(nolower(0x6E), Err(0x6E));

            let noeither = |v| {
                translations!(
                    v => <u8 => i16>
                    => trans -0x20 => 0xCF
                    failmode result
                )
            };

            assert_eq!(noeither(0x00), Ok(-0x20));
            assert_eq!(noeither(0xFF), Ok(0xCF));
        }
    }

    /// Testing alternate failure modes - result, option, panic
    mod errmodes {
        #[test]
        fn result_failure_mode() {
            let trans = |v| {
                translations!(
                    v => <u16 => i16>
                        0 => 255 trans down 0 => 255
                    failmode result
                )
            };

            assert_eq!(trans(256), Err(256));
            assert_eq!(trans(1), Ok(254));
        }

        #[test]
        fn option_failure_mode() {
            let trans = |v| {
                translations!(
                    v => <u16 => i16>
                        0 => 255 trans down 0 => 255
                    failmode option
                )
            };

            assert_eq!(trans(256), None);
            assert_eq!(trans(1), Some(254));
        }
    }
}*/

//  Copyright © 2022 <sapient_cogbag at protonmail dot com>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy of this software
//  and associated documentation files (the “Software”), to deal in the Software without
//  restriction, including without limitation the rights to use, copy, modify, merge, publish,
//  distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
//  Software is furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all copies or
//  substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
//  BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.}
