//! For traits that should probably be in [core] or something like that but aren't, such as a trait
//! for numerical types that have a minimum, maximum, and viable zero value.

/// Tag trait indicating a numerical type is unsigned. When it implements [MinMaxNil], this
/// indicates that [MinMaxNil::MIN] == [MinMaxNil::ZERO].
pub trait Unsigned {}

macro_rules! unsigned_impl {
    ($($ty:ty)*) => {$(
        impl Unsigned for $ty {}
    )*}
}

unsigned_impl! {u8 u16 u32 u64 u128 usize bool}

/// Simple trait indicating min/max/zero for a type
///
/// Implement this to get sensible defaults for range inputs when not using a custom
/// [crate::bounds::DefaultInputBounds] trait.
///
/// Implementers MUST ENSURE that [MinMaxNil::MIN] <= [MinMaxNil::ZERO], and that
/// [MinMaxNil::ZERO] <= [MinMaxNil::MAX]
pub trait MinMaxNil: PartialOrd + PartialEq {
    const MIN: Self;
    const MAX: Self;
    const ZERO: Self;
}

/// impls for all the standard thingies
macro_rules! minmaxnil_impl {
    ($($type:ty),*) => {$(
        impl MinMaxNil for $type {
            const MIN: Self = <$type>::MIN;
            const MAX: Self = <$type>::MAX;
            const ZERO: Self = 0 as $type;
        }
    )*}
}

minmaxnil_impl! {
    f32, f64,
    u8, u16, u32, u64, u128, usize,
    i8, i16, i32, i64, i128, isize
}

impl MinMaxNil for bool {
    const MIN: Self = Self::ZERO;
    const MAX: Self = true;
    const ZERO: Self = false;
}

/// Trait for an item having wrapping `+` and provide it as a function.
pub trait WrappingAdd<RHS = Self> {
    type Output;

    /// Wrapping addition function
    fn wrapping_add(self, other: RHS) -> Self::Output;
}

/// Trait for an item having wrapping `-` and provide it as a function.
pub trait WrappingSub<RHS = Self> {
    type Output;

    /// Wrapping subtraction function
    fn wrapping_sub(self, other: RHS) -> Self::Output;
}

macro_rules! wrapping_addsub_impl {
    ($($wty:ty)*) => {$(
        impl WrappingAdd for $wty {
            type Output = $wty;

            #[inline]
            fn wrapping_add(self, other: Self) -> Self::Output {
                self.wrapping_add(other)
            }
        }

        impl WrappingSub for $wty {
            type Output = $wty;

            #[inline]
            fn wrapping_sub(self, other: Self) -> Self::Output {
                self.wrapping_sub(other)
            }
        }
    )*}
}

wrapping_addsub_impl! { usize u8 u16 u32 u64 u128 isize i8 i16 i32 i64 i128 }

/// Trait for an item having saturating `+` and provide it as a function.
pub trait SaturatingAdd<RHS = Self> {
    type Output;

    /// Saturating addition function
    fn saturating_add(self, other: RHS) -> Self::Output;
}

/// Trait for an item having saturating `-` and provide it as a function.
pub trait SaturatingSub<RHS = Self> {
    type Output;

    /// Saturating subtraction function
    fn saturating_sub(self, other: RHS) -> Self::Output;
}

macro_rules! saturating_addsub_impl {
    ($($wty:ty)*) => {$(
        impl SaturatingAdd for $wty {
            type Output = $wty;

            #[inline]
            fn saturating_add(self, other: Self) -> Self::Output {
                self.saturating_add(other)
            }
        }

        impl SaturatingSub for $wty {
            type Output = $wty;

            #[inline]
            fn saturating_sub(self, other: Self) -> Self::Output {
                self.saturating_sub(other)
            }
        }
    )*}
}

saturating_addsub_impl! { usize u8 u16 u32 u64 u128 isize i8 i16 i32 i64 i128 }

//  Copyright © 2022 <sapient_cogbag at protonmail dot com>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy of this software
//  and associated documentation files (the “Software”), to deal in the Software without
//  restriction, including without limitation the rights to use, copy, modify, merge, publish,
//  distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
//  Software is furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all copies or
//  substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
//  BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.}
