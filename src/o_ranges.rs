//! Same as [crate::i_bounds] but for the output regions.
//!
//! TODO: Implement region length checking where possible:
//!  * "Is length preserved" bool function with provided input length for [crate::preconv::PreConversion]
//!  * "Output range length -> Input range length" function on [crate::delta::DeltaApplicationOp],
//!  preferrably with errorbounds for cases where the "rate of change" is not a dicrete integer.
//!  * Both should produce [Option] because implementors may be unwilling or unable to implement
//!  them.
//!
//!  Note that for [OutputRange], inclusive vs exclusive bounds do not make much sense. For
//!  consistency, we only allow specification via `(?..?)`, with no equals sign.

use crate::{ranges::DefaultOutputRange, rhs_tag::reverse_or_not, rhs_tag::TargetRangeDirection};

/// Generic method of applying an output range to a delta with a given
/// [crate::rhs_tag::TranslationTag] for direction indication, and with a given operation.
pub trait OutputRange<TargetType> {
    /// Apply the given final application operation on the correct target value. The final
    /// operation is passed as a closure (FnOnce) taking a Target and
    /// producing another Target.
    ///
    /// Note for now that the direction of the operation is dictated by the `IS_REVERSED` const
    /// generic parameter which is the output of `rhs_tag::is_reversed_translation` (private).
    /// When ADTs as const generics are stabilised, we will switch to using
    /// [crate::rhs_tag::TargetRangeDirection] emum directly ^.^
    ///
    /// SAFETY REQUIREMENTS:
    /// * must be directly
    unsafe fn apply_operation_in_direction<
        Defaults: DefaultOutputRange<TargetType>,
        const IS_REVERSED: bool,
    >(
        &self,
        apply_delta: impl FnOnce(TargetType) -> TargetType,
    ) -> TargetType;
}

/// start..end, don't use defaults.
// meow ^.^
impl<T: Copy> OutputRange<T> for core::ops::Range<T> {
    #[inline]
    unsafe fn apply_operation_in_direction<
        Defaults: DefaultOutputRange<T>,
        const IS_REVERSED: bool,
    >(
        &self,
        apply_delta: impl FnOnce(T) -> T,
    ) -> T {
        match reverse_or_not(IS_REVERSED) {
            TargetRangeDirection::Forward => apply_delta(self.start),
            TargetRangeDirection::Reverse => apply_delta(self.end),
        }
    }
}

/// start.., use defaults on end.
impl<T: Copy> OutputRange<T> for core::ops::RangeFrom<T> {
    #[inline]
    unsafe fn apply_operation_in_direction<
        Defaults: DefaultOutputRange<T>,
        const IS_REVERSED: bool,
    >(
        &self,
        apply_delta: impl FnOnce(T) -> T,
    ) -> T {
        match reverse_or_not(IS_REVERSED) {
            TargetRangeDirection::Forward => apply_delta(self.start),
            TargetRangeDirection::Reverse => apply_delta(Defaults::DEFAULT_RANGE_END),
        }
    }
}

// meow ^.^
/// ..end, use default start ^.^
impl<T: Copy> OutputRange<T> for core::ops::RangeTo<T> {
    #[inline]
    unsafe fn apply_operation_in_direction<
        Defaults: DefaultOutputRange<T>,
        const IS_REVERSED: bool,
    >(
        &self,
        apply_delta: impl FnOnce(T) -> T,
    ) -> T {
        match reverse_or_not(IS_REVERSED) {
            TargetRangeDirection::Forward => apply_delta(Defaults::DEFAULT_RANGE_START),
            TargetRangeDirection::Reverse => apply_delta(self.end),
        }
    }
}

/// (..), use defaults on both ends ^.^
impl<T> OutputRange<T> for core::ops::RangeFull {
    #[inline]
    unsafe fn apply_operation_in_direction<
        Defaults: DefaultOutputRange<T>,
        const IS_REVERSED: bool,
    >(
        &self,
        apply_delta: impl FnOnce(T) -> T,
    ) -> T {
        match reverse_or_not(IS_REVERSED) {
            TargetRangeDirection::Forward => apply_delta(Defaults::DEFAULT_RANGE_START),
            TargetRangeDirection::Reverse => apply_delta(Defaults::DEFAULT_RANGE_END),
        }
    }
}

//  Copyright © 2022 <sapient_cogbag at protonmail dot com>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy of this software
//  and associated documentation files (the “Software”), to deal in the Software without
//  restriction, including without limitation the rights to use, copy, modify, merge, publish,
//  distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
//  Software is furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all copies or
//  substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
//  BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.}
