//! Dictates the way in which a change in the source converts to a change in the target.
use crate::delta::HasPositiveDelta;

/// At some point in the translation, you need to convert a change in the value of the source
/// type into a change in the value of the target type. Types implementing this trait dictates
/// how to do that.
pub trait PreConversion<Source: HasPositiveDelta, TargetDelta> {
    fn convert(positive_source_delta: Source::PositiveDelta) -> TargetDelta;
}

/// Tag type for conversion via the `as` operator.
///
/// For types like [u16] being converted to types like [u8] that are smaller, this will
/// truncate in a [sensible manner](https://doc.rust-lang.org/reference/expressions/operator-expr.html#type-cast-expressions)
pub struct AsOpConversion;

// Implementation macro per-type with the `as`-able target delta types, for int + f32/64
// (noting that we cannot convert floats *back* to ints here nya
macro_rules! impl_asconv_for_numeric_primitives{
    (@single_set $src:ty => $($tgts:ty)*) => {$(
        impl PreConversion<$src, $tgts> for AsOpConversion {
            #[inline]
            fn convert(positive_source_delta: <$src as $crate::delta::HasPositiveDelta>::PositiveDelta) -> $tgts {
                positive_source_delta as $tgts
            }
        }
    )*};
    ($($srcs:ty)*) => {$(
        // Rust does not like using repeating vars at any depth below where they are defined
        // >.< nya
        //
        // Otherwise I'd just reuse the sources.
        impl_asconv_for_numeric_primitives!{
            @single_set $srcs =>
                u8 u16 u32 u64 u128 usize
                i8 i16 i32 i64 i128 isize
                f32 f64
        }
    )*}
}

impl_asconv_for_numeric_primitives! {
    u8 u16 u32 u64 u128 usize
    i8 i16 i32 i64 i128 isize
}

// Special definitions for fp
impl_asconv_for_numeric_primitives! {@single_set f32 => f32 f64}
impl_asconv_for_numeric_primitives! {@single_set f64 => f32 f64}

/// Like [AsOpConversion], but instead converts any pair of types where the
/// [crate::delta::HasPositiveDelta::PositiveDelta] of the first implements [Into] for the
/// second type.
pub struct IntoOpConversion;

impl<Source, TargetDelta> PreConversion<Source, TargetDelta> for IntoOpConversion
where
    Source: HasPositiveDelta,
    <Source as HasPositiveDelta>::PositiveDelta: Into<TargetDelta>,
{
    #[inline]
    fn convert(positive_source_delta: Source::PositiveDelta) -> TargetDelta {
        // nya
        positive_source_delta.into()
    }
}

/// No-op conversion that takes any types where [HasPositiveDelta::PositiveDelta] of the first
/// *is* the second type.
pub struct NoOpConversion;

impl<Source: HasPositiveDelta> PreConversion<Source, Source::PositiveDelta> for NoOpConversion {
    #[inline]
    fn convert(positive_source_delta: Source::PositiveDelta) -> Source::PositiveDelta {
        positive_source_delta
    }
}

//  Copyright © 2022 <sapient_cogbag at protonmail dot com>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy of this software
//  and associated documentation files (the “Software”), to deal in the Software without
//  restriction, including without limitation the rights to use, copy, modify, merge, publish,
//  distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
//  Software is furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all copies or
//  substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
//  BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.}
