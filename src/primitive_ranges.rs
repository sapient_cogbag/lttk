//! Macro for checking ranges (including boundary points) of values in a type en-masse
//! for the purpose of constructing character classes. Also allows optional manual type
//! specification.
//!
//! This macro is very basic compared to the primary function of this library, but it is often
//! enough for many things.
//!
//! # SYNTAX
//!
//! ```
//! ranges(
//!     <variable-identifier> in <[optional-type]>
//!     [lower-bound-literal] ~ [upper-bound-literal],...
//! )
//! ```
//!
//! # EXAMPLES
//!
//! Hex digit identifier
//! ```rust
//! use lttk::ranges;
//!
//! pub const fn is_hex_digit(chbyte: u8) -> bool {
//!     ranges!(chbyte in b'A' ~ b'Z', b'a' ~ b'z', b'0' ~ b'9')
//! }
//!
//! assert!(is_hex_digit(b'5'))
//! assert!(!is_hex_digit(b'q'))
//! assert!(is_hex_digit(b'a'))
//! ```
//!
//! ASCII identifier
//! ```rust
//! use lttk::ranges;
//!
//! pub const fn is_ascii_byte(chbyte: u8) -> bool {
//!     ranges!(chbyte in ~ 0x7F)
//! }
//!
//! assert!(is_ascii_byte(b'a'))
//! assert!(!is_ascii_byte(0x80))
//! assert!(is_ascii_byte(0x7F))
//! ```

#[macro_export]
macro_rules! ranges {
    ($var:ident in $($($lbinc:literal)? ~ $($ubinc:literal)?),*) => {{
        $(ranges!(@ $var in $($lbinc)? ~ $($ubinc)?))||*
    }};
    ($var:ident in <$type:ty> $($($lbinc:literal)? ~ $($ubinc:literal)?),*) => {{
        $(ranges!(@ $var in <$type> $($lbinc)? ~ $($ubinc)?))||*
    }};
    (@ $var:ident in $(<$type:ty>)? ~ ) => { (true) };
    (@ $var:ident in $(<$type:ty>)? $lbinc:literal ~) => { ($lbinc $(as $type)? <= $var) };
    (@ $var:ident in $(<$type:ty>)? ~ $ubinc:literal) => { ($var <= $ubinc $(as $type)?) };
    (@ $var:ident in $(<$type:ty>)? $lbinc:literal ~ $ubinc:literal) => {
        ($lbinc $(as $type)? <= $var && $var <= $ubinc $(as $type)?)
    };
}

#[cfg(test)]
mod ranges_tests {
    fn is_ascii_byte(b: u8) -> bool {
        ranges!(b in <u8> ~ 0x7F)
    }

    #[test]
    fn ranges_upper_bound_only() {
        assert!(is_ascii_byte(0));
        assert!(is_ascii_byte(0x7F));
        assert!(!is_ascii_byte(0x80));
        assert!(is_ascii_byte(b'a'));
    }

    fn is_unicode_byte(b: u8) -> bool {
        ranges!(b in 0x80 ~ )
    }

    #[test]
    fn ranges_lower_bound_only() {
        for byte in 0..=255u8 {
            assert_ne!(is_ascii_byte(byte), is_unicode_byte(byte));
        }
    }

    /// Don't use this in real implementations
    fn is_capital_ascii(b: u8) -> bool {
        ranges!(b in b'A' ~ b'Z')
    }

    /// Don't use this in real implementations.
    fn is_alphabetical_ascii(b: u8) -> bool {
        ranges!(b in b'A' ~ b'Z', b'a' ~ b'z')
    }

    #[test]
    fn ranges_both_ends() {
        for byte in b'A'..=b'Z' {
            assert!(is_capital_ascii(byte))
        }
        for byte in (0..b'A').into_iter().chain((b'Z' + 1)..u8::MAX) {
            assert!(!is_capital_ascii(byte))
        }
    }

    #[test]
    fn multi_ranges() {
        for byte in (b'A'..=b'Z').into_iter().chain((b'a'..=b'z').into_iter()) {
            assert!(is_alphabetical_ascii(byte))
        }
        for byte in (0..b'A')
            .into_iter()
            .chain(((b'Z' + 1)..b'a').into_iter())
            .chain(((b'z' + 1)..=u8::MAX).into_iter())
        {
            assert!(!is_alphabetical_ascii(byte));
        }
    }
}
