//! Like [crate::bounds] in that this submodule defines traits for establishing sensible defaults
//! for when an upper or lower translated-bound does not exist.
use crate::numprops::{MinMaxNil};

/// Indicates the default values for an output range when not specified, for a type.
///
/// Note that because bad conditionals are much more costly than bad additions, we provide less
/// effort to optimise away unnecessary adds/subs like with
/// [crate::bounds::DefaultInputBounds::NEEDS_LOWER_BOUND_CHECK] and
/// [crate::bounds::DefaultInputBounds::NEEDS_UPPER_BOUND_CHECK]. Instead, we trust the compiler to
/// figure things out more ^.^
pub trait DefaultOutputRange<T> {
    const DEFAULT_RANGE_START: T;
    const DEFAULT_RANGE_END: T;
}

/// Tag type implementing [DefaultOutputRange] for types implementing [MinMaxNil], with the default
/// range start as min and default range end as max.
pub struct MinMaxDefaultOutputRange;

impl<T: MinMaxNil + PartialEq + PartialOrd> DefaultOutputRange<T> for MinMaxDefaultOutputRange {
    const DEFAULT_RANGE_START: T = T::MIN;
    const DEFAULT_RANGE_END: T = T::MAX;
}

/// Tag type implementing [DefaultOutputRange] for types implementing [MinMaxNil], with the default
/// range start as zero and default range end as max.
pub struct ZeroMaxDefaultOutputRange;

impl<T: MinMaxNil + PartialEq + PartialOrd> DefaultOutputRange<T> for ZeroMaxDefaultOutputRange {
    const DEFAULT_RANGE_START: T = T::ZERO;
    const DEFAULT_RANGE_END: T = T::MAX;
}

//  Copyright © 2022 <sapient_cogbag at protonmail dot com>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy of this software
//  and associated documentation files (the “Software”), to deal in the Software without
//  restriction, including without limitation the rights to use, copy, modify, merge, publish,
//  distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
//  Software is furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all copies or
//  substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
//  BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.}
