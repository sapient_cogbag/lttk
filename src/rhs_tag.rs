//! For tagging conversions with their direction (forward for `+` from the bottom, or reverse for `-` from the top)
use crate::delta::DeltaApplicationOp;

#[derive(Eq, PartialEq)]
/// Represents which direction deltas should be applied to a range.
pub enum TargetRangeDirection {
    /// For a range `a..b`, this applies the delta in the forward (positive, usually)
    /// direction to `a`, i.e. smaller delta = closer to `a` for conventional-direction
    /// [crate::delta::DeltaApplicationOp]
    Forward,
    /// For a range `a..b`, this applies the delta in the reverse (negative, usually) direction
    /// to `b`, i.e. smaller delta = closer to `b` for conventional direction
    /// [crate::delta::DeltaApplicationOp]
    Reverse,
}


/// Whether or not a [TargetRangeDirection] indicates that a range is reversed or not.
///
/// Primarily used because ADT types in const rust are not yet stable.
pub const fn is_reversed_translation(trd: TargetRangeDirection) -> bool {
    match trd {
        TargetRangeDirection::Forward => false,
        TargetRangeDirection::Reverse => true,
    }
}


/// If true, reverse. If false, don't. Inverse function of [is_reversed_translation]
pub const fn reverse_or_not(is_reversed: bool) -> TargetRangeDirection {
    match is_reversed {
        true => TargetRangeDirection::Reverse,
        false => TargetRangeDirection::Forward,
    }
}

/// Trait for the RHS of a specific translation - i.e. specifying whether the translation is
/// forward (lowerout+) or inverted (upperout-).
pub trait TranslationTag {
    /// Indicate the direction this tag translates.
    const DIRECTION: TargetRangeDirection;
    /// Indicate the direction this tag translates with a const-generics-compatible type (bool).
    /// True indicates that the direction is reversed, false means not.
    ///
    /// Interface WILL CHANGE to not include this when ADT types are allowed in const generics cus
    /// then we can use the [TargetRangeDirection] enum *directly* ^.^
    const IS_REVERSED: bool;
}

/// Tag indicating that a delta should be applied from the lower output bound upward.
#[derive(Default, Clone, Copy)]
pub struct Forward;

/// Tag indicating that a delta should be applied from the upper output bound downward.
#[derive(Default, Clone, Copy)]
pub struct Reverse;

/// TODO: When ADT types in const are stabilised, use [TargetRangeDirection] enum directly rather
/// than output from [is_reversed_translation] enum in the generic parameters.
///
/// This function will perform one half of the direction related stuff - selecting which direction
/// of [crate::delta::DeltaApplicationOp] to use. The other half is performed in
/// [crate::o_ranges::OutputRange::apply_operation_in_direction].
///
/// SAFETY REQUIREMENTS:
///  * called with the same directionality as the corresponding
///  [crate::o_ranges::OutputRange::apply_operation_in_direction]
#[inline]
pub(crate) unsafe fn translate_delta_partial<
    'appop_and_delta,
    const IS_REVERSED: bool,
    Target,
    TransOp: DeltaApplicationOp<Target>,
>(
    operation: &'appop_and_delta TransOp,
    delta: TransOp::Delta,
) -> impl FnOnce(Target) -> Target + 'appop_and_delta
where
    TransOp::Delta: 'appop_and_delta,
{
    #[inline]
    move |baseline_target: Target| {
        if IS_REVERSED {
            // second half is in the output range op
            operation.sub_delta(baseline_target, delta)
        } else {
            // second half is in the output range op which must apply the correct target nya
            operation.add_delta(baseline_target, delta)
        }
    }
}

impl TranslationTag for Forward {
    const DIRECTION: TargetRangeDirection = TargetRangeDirection::Forward;
    const IS_REVERSED: bool = false;
}

impl TranslationTag for Reverse {
    const DIRECTION: TargetRangeDirection = TargetRangeDirection::Reverse;
    const IS_REVERSED: bool = true;
}

//  Copyright © 2022 <sapient_cogbag at protonmail dot com>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy of this software
//  and associated documentation files (the “Software”), to deal in the Software without
//  restriction, including without limitation the rights to use, copy, modify, merge, publish,
//  distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
//  Software is furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all copies or
//  substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
//  BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
//  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.}
